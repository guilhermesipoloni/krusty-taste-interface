require "rails_helper"

RSpec.describe RecipesController, type: :controller do

  describe '#public_index' do
    before do
      VCR.use_cassette("GET_public_index") do
        get :public_index
      end
    end
    it{ expect(response).to render_template(:public_index) }
  end

  describe '#private_index' do
    before do
      VCR.use_cassette("GET_private_index") do
        session[:auth_token] = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MjYwNjMzMTYsInN1YiI6MjB9.3X6KdAOZ5Lvipnca59c3QDUgttM07thXtAoKCUNigD8"
        get :private_index
      end
    end
    it{ expect(response).to render_template(:private_index) }
  end

  describe '#show' do
    before do
      VCR.use_cassette("GET_show") do
        get :show, params: {id: 15}
      end
    end
    it{ expect(response).to render_template(:show) }
  end

  describe '#create' do
    let(:params) { { name: "burger", description: "the burger", public: true, ingredients_attributes: [{ name: "salt", measurement: "1 spoon"}, { name: "pepper", measurement: "1 spoon"}], preparation_stepsattributes:[{step: 1, description: "mix all"}]} }
    before do
      VCR.use_cassette("POST_create") do
        session[:auth_token] = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MjYwNjMzMTYsInN1YiI6MjB9.3X6KdAOZ5Lvipnca59c3QDUgttM07thXtAoKCUNigD8"
        post :create, params: params
      end
    end
    it{ expect(response).to redirect_to(recipes_private_path) }
  end

  describe '#update' do
    let(:params) { { id: 17, name: "hamburger" } }
    before do
      VCR.use_cassette("PUT_update") do
        session[:auth_token] = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MjYwNjMzMTYsInN1YiI6MjB9.3X6KdAOZ5Lvipnca59c3QDUgttM07thXtAoKCUNigD8"
        put :update, params: params
      end
    end
    it{ expect(response).to redirect_to('/recipes/17') }
  end

  describe '#destroy' do
    let(:params) { { id: 17 } }
    before do
      VCR.use_cassette("DELETE_delete") do
        session[:auth_token] = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MjYwNjMzMTYsInN1YiI6MjB9.3X6KdAOZ5Lvipnca59c3QDUgttM07thXtAoKCUNigD8"
        delete :destroy, params: params
      end
    end
    it{ expect(response).to redirect_to(recipes_private_path) }
  end

end