function addField(name, field1, field2) {
  var date = new Date();
  var mSec = date.getTime();

  idAttribut1 = 
         ("recipe_"+name+"_0_"+field1).replace("0", mSec);
  nameAttribut1 = 
         "["+name+"_attributes][]["+field1+"]"

  idAttribut2 = 
         ("recipe_"+name+"_0_"+field2).replace("0", mSec);
  nameAttribut2 = 
         "["+name+"_attributes][]["+field2+"]"

  var li = document.createElement("li");           

  //create input for Kind, set it's type, id and name attribute, 
  //and append it to <li> element  
  var input1 = document.createElement("INPUT");
  input1.setAttribute("type", "text");
  input1.setAttribute("id", idAttribut1);
  input1.setAttribute("name", nameAttribut1);
  li.appendChild(input1);

  var input2 = document.createElement("INPUT");
  input2.setAttribute("type", "text");
  input2.setAttribute("id", idAttribut2);
  input2.setAttribute("name", nameAttribut2);
  li.appendChild(input2);

  //add created <li> element with its child elements 
  //(label and input) to myList (<ul>) element
  document.getElementById(name+"_list").appendChild(li);
}