class UsersController < ApplicationController

  def create
    @user =  JSON.parse(User.create(params))
    flash[:notice] = "User created"
    redirect_to root_path
  rescue Exception => e
    flash[:notice] = e.to_s
    render :new
  end

  def new
  end

  def access
  end

  def login
    response = JSON.parse(User.login(params))
    flash[:notice] = "User logged-in"
    session[:auth_token] = response['jwt']
    redirect_to recipes_private_path
  rescue Exception => e
    flash[:notice] = e.to_s
    render :access
  end

  def logoff
    session[:auth_token] = nil
    redirect_to root_path
  end
end