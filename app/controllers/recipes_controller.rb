require 'will_paginate/array'
class RecipesController < ApplicationController

  def public_index

    limit = params[:limit] ? params[:limit].to_i : 10
    page = params[:page] ? params[:page].to_i : 1
    offset = (page - 1) * limit
    params[:offset] = offset
    params[:limit] = limit

    response = JSON.parse(Recipe.public_recipes(params))
    @recipes = response['recipes']
    @paginate = response['recipes'].paginate(page: page, per_page: limit, total_entries: response['summary']['total'])
    render :public_index
  end

  def private_index
    limit = params[:limit] ? params[:limit].to_i : 10
    page = params[:page] ? params[:page].to_i : 1
    offset = (page - 1) * limit
    params[:offset] = offset
    params[:limit] = limit

    response = JSON.parse(Recipe.private_recipes(params, session[:auth_token]))
    @recipes = response['recipes']
    @paginate = response['recipes'].paginate(page: page, per_page: limit, total_entries: response['summary']['total'])
    render :private_index
  end

  def show
    @recipe = JSON.parse(Recipe.details(params, session[:auth_token]))
    render :show
  end

  def create
    params[:image] = has_image?
    @recipe = JSON.parse(Recipe.create(params, session[:auth_token]))
    redirect_to recipes_private_path
  rescue Exception => e
    flash[:notice] = JSON.parse(e.to_s)['message']
    render :new
  end

  def new
  end

  def update
    @recipe = JSON.parse(Recipe.details(params, session[:auth_token]))
    JSON.parse(Recipe.update(params, session[:auth_token]))
    redirect_to recipe_path(params[:id])
  rescue Exception => e
    flash[:notice] = JSON.parse(e.to_s)['message']
    render :edit
  end

  def edit
    @recipe = JSON.parse(Recipe.details(params, session[:auth_token]))
  end

  def destroy
    @recipe = JSON.parse(Recipe.delete(params, session[:auth_token]))
    redirect_to recipes_private_path
  end

  private
  def has_image?
    unless params[:image].blank?
      return "data:#{params[:image].content_type};base64,#{Base64.encode64(params[:image].tempfile.open.read.force_encoding(Encoding::UTF_8))}"
    end
    File.open(File.expand_path('.')+"/app/assets/images/no_image.txt").read
  end
end