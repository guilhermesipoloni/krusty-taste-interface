require 'http'
class Recipe

  def self.public_recipes(params)
    response = HTTP.get("#{ENV['KRUSTY-TASTE-API-URI']}/recipes", params: params.to_h)
    return response
  end

  def self.private_recipes(params, auth_token)
    response = HTTP.auth("Bearer #{auth_token}").get("#{ENV['KRUSTY-TASTE-API-URI']}/recipes/private", params: params.to_h)
    if response.code == 401
      return "{}"
    end
    response
  end

  def self.details(params, auth_token)
    response = HTTP.auth("Bearer #{auth_token}").get("#{ENV['KRUSTY-TASTE-API-URI']}/recipes/#{params[:id]}")
    if response.code == 401
      return "{}"
    end
    response
  end

  def self.create(params, auth_token)
    response = HTTP.auth("Bearer #{auth_token}").post("#{ENV['KRUSTY-TASTE-API-URI']}/recipes", json: params.to_h)
    if response.code == 401
      return "{}"
    end
    if response.code == 400
      raise Exception.new(response)
    end
    response
  end

  def self.update(params, auth_token)
    response = HTTP.auth("Bearer #{auth_token}").put("#{ENV['KRUSTY-TASTE-API-URI']}/recipes/#{params[:id]}", json: params.to_h)
    if response.code == 401
      return "{}"
    end
    if response.code == 400
      raise Exception.new(response)
    end
    response
  end

  def self.delete(params, auth_token)
    response = HTTP.auth("Bearer #{auth_token}").delete("#{ENV['KRUSTY-TASTE-API-URI']}/recipes/#{params[:id]}")
    if response.code == 401
      return "{}"
    end
    if response.code == 400
      raise Exception.new(response)
    end
    response
  end

end
