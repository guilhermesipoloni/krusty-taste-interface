require 'http'
class User

  def self.create(params)
    response = HTTP.post("#{ENV['KRUSTY-TASTE-API-URI']}/users", json: params.to_h) 
    if response.code == 201
      return response
    end
    if response.code == 400
      raise Exception.new(response)
    end
  end

  def self.login(params)
    response = HTTP.post("#{ENV['BASE-URI-LOGIN']}", json: params.to_h) 
    if response.code == 201
      return response
    end
    "{message: 'User or password invalid'}"
  end
end