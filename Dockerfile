FROM ruby:2.5.1

RUN apt-get update && \
    apt-get install -y net-tools

ENV APP_HOME /app
ENV HOME /root

RUN mkdir $APP_HOME
COPY . $APP_HOME

WORKDIR $APP_HOME

RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - \
        && apt-get install -y nodejs

RUN gem install bundler

RUN bundle install

EXPOSE 3001
CMD ["rails", "s", "-b", "0.0.0.0", "-p", "3001"]