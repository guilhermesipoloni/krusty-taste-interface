Rails.application.routes.draw do
  root 'recipes#public_index'
  get 'recipes/private', to: 'recipes#private_index'

  get 'users/access', to: 'users#access'
  post 'users/login', to: 'users#login'
  get 'users/logoff', to: 'users#logoff'

  resources :users, only:[:create, :new]
  resources :recipes 
end
