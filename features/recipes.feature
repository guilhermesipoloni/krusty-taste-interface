Feature: Recipes
  In order to make a recipe
  As a cooker
  I want to see a recipe in krusty taste

  Scenario: Recipes list
    Given I am in the home page
    Then I should see "poutine"

  Scenario: Recipes list
    Given I am in the home page
    When I click in "poutine"
    Then I should see "Ingredients"
    And I should see "Preparation Steps"
