Given("I am in the home page") do
  visit root_path
end

When(/^I click in "([^\"]*)"$/) do |text|
  click_on text
end

Then /^I should see "([^\"]*)"$/ do |text|
  expect(page).to have_content text
end