# krusty-taste-interface

[![CircleCI](https://circleci.com/bb/guilhermesipoloni/krusty-taste-interface.svg?style=svg)](https://circleci.com/bb/guilhermesipoloni/krusty-taste-interface)

Krusty taste interface is an interface for [krusty-taste-api](https://bitbucket.org/guilhermesipoloni/krusty-taste-api/src)

[Krustytaste](https://krusty-taste-interface.herokuapp.com/)

##Run Docker 

**Build docker image:** `docker-compose build`

**Build database:** `docker-compose up postgresql`

**Setup database:** `docker-compose run --rm web rake db:setup`

**Start application:** `docker-compose up`

**Run tests:** `docker-compose run --rm web bundle exec rspec`

##Deploy

**Automagic:** When master has a new commit or merge and all the tests is passing the deploy process will start automatically.